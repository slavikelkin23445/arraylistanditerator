package org.example.MyArrayList;

import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.Iterator;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class MyArrayList<T> implements Iterable<T>{

    private final static int DEFAULT_CAPACITY = 16;
    private final static double SCALE_FACTOR = 1.5;

    private Object[] arr;
    private int size = 0;

    public MyArrayList() {
        this(DEFAULT_CAPACITY);
    }

    public MyArrayList(int capacity) {
        this.arr = new Object[capacity];
    }

    public int size(){
        return size;
    }

    public T get(int index){
        checkIndex(index);
        return (T)arr[index];
    }

    public void set(int index, T value){
        checkIndex(index);
        arr[index] = value;
    }

    public void checkIndex(int index){
        if(index >= size || index < 0) throw new IndexOutOfBoundsException(index);
    }

    public void add(T value){
        if(size >= arr.length) expand();
        arr[size++] = value;
    }

    private void expand() {
        int newCapacity = (int)(arr.length*SCALE_FACTOR+1);
        arr = Arrays.copyOf(arr, newCapacity);
    }

    public void addAll(MyArrayList<T> list){
        for (int i = 0; i < list.size(); i++) {
            add(list.get(i));
        }
    }

    public boolean every(Predicate<T> predicate){
        for(int i = 0; i < size; i++){
            if(predicate.test((T)arr[i])){
                continue;
            }else{
                return false;
            }
        }
        return true;
    }

    public boolean some(Predicate<T> predicate){
        for(int i = 0; i < size; i++){
            if(predicate.test((T)arr[i])){
                return true;
            }
        }
        return false;
    }

    <R> R reduce(R initValue, BiFunction<T,R,R> f){
        R result = initValue;
        for (int i = 0; i < size; i++) {
            result = f.apply((T)arr[i],result);
        }
        return result;
    }
    private class MyReversedIterator<T> implements Iterator<T>{
        private int index = size - 1;
        @Override
        public boolean hasNext() {
            return index >= 0;
        }

        @Override
        public T next() {
            return (T)arr[index--];
        }
        @Override
        public void forEachRemaining(Consumer action) {
            Iterator.super.forEachRemaining(action);
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            private int index = 0;
            public boolean hasNext() {
                return index < size;
            }
            public T next() {
                return (T)arr[index++];
            }
        };
    }

    public Iterator<T> reversedIterator() {
        return new MyReversedIterator<>();
        }
    }

class Main{
    public static void main(String[] args) {

        MyArrayList<Integer> arr = new MyArrayList<>();
        arr.add(1);
        arr.add(2);
        arr.add(4);
        arr.add(5);
//        arr.forEach(i -> System.out.println("-> " + i));
//        System.out.println("-----------------------------------------------");
//        arr.reversedIterator().forEachRemaining(i -> System.out.println("-> " + i));
    }
}
