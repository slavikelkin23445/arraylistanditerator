package org.example.MyArrayList;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class MyArrayListTest {

    @Test
    public void TestIterator(){
        MyArrayList<Integer> arr = new MyArrayList<>();

        arr.add(1);
        arr.add(2);
        arr.add(3);
        arr.add(4);
        arr.add(5);

        assertEquals(5,arr.size());
        assertEquals(3, arr.get(2));
    }
    @Test
    public void TestEvery(){
        MyArrayList<Integer> arr = new MyArrayList<>();

        arr.add(10);
        arr.add(20);
        arr.add(30);
        arr.add(40);
        arr.add(50);

        assertEquals(true, arr.every(e->e>=10));

        arr.set(2,9);

        assertEquals(false, arr.every(e->e>=10));
    }
    @Test
    public void TestSome(){
        MyArrayList<Integer> arr = new MyArrayList<>();

        arr.add(1);
        arr.add(20);
        arr.add(3);
        arr.add(40);
        arr.add(50);

        assertEquals(true, arr.some(e->e>=10));

        MyArrayList<Integer> arr2= new MyArrayList<>();

        arr.add(1);
        arr.add(2);
        arr.add(3);
        arr.add(4);
        arr.add(5);

        assertEquals(false, arr2.some(e->e>=10));
    }
    @Test
    public void TestOutOfBound(){
        MyArrayList<Integer> arr = new MyArrayList<>();

        arr.add(1);
        arr.add(2);
        arr.add(3);
        arr.add(4);
        arr.add(5);

        assertThrows(IndexOutOfBoundsException.class, ()->arr.get(5));
        assertThrows(IndexOutOfBoundsException.class, ()->arr.get(-1));
        assertThrows(IndexOutOfBoundsException.class, ()->arr.get(8));
    }
    @Test
    public void TestReduce(){
        MyArrayList<Integer> arr = new MyArrayList<>();
        arr.add(1);
        arr.add(2);
        arr.add(4);
        arr.add(5);

        assertEquals(12, arr.reduce(0,(c,p)-> c+p));
        assertEquals(40, arr.reduce(1,(c,p)-> c*p));
    }
}